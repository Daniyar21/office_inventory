const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
  const [places] = await mysqlDb.getConnection().query('SELECT id, title  FROM ??', ['places']);
  res.send(places);
});


router.get('/:id', async (req, res) => {
  const [place] = await mysqlDb.getConnection().query(
    `SELECT * FROM ?? where id = ?`,
    ['places', req.params.id])
  if (!place) {
    return res.status(404).send({error: 'Place is  not found'});
  }

  res.send(place[0]);
});


router.post('/', async (req, res) => {
  if (!req.body.title) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const place = {
    title: req.body.title,
    description: req.body.description,
  };


  const newPlace = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (title, description) values (?, ?)',
    ['places', place.title, place.description]
  );

  res.send({
    ...place,
    id: newPlace[0].insertId
  });
});

router.delete('/:id', async (req, res) => {
  const [place] = await mysqlDb.getConnection().query(
    'DELETE FROM ?? where id = ?',
    ['places', req.params.id])
  if (!place) {
    return res.status(404).send({error: 'Data not found'});}

  res.send('Done');
});


router.put('/:id', async (req, res) => {
  const place = {
    title: req.body.title,
    description: req.body.description,
  };

  await mysqlDb.getConnection().query(
    'UPDATE ?? SET ? where id = ?',
    ['places', {...place}, req.params.id]);

  res.send({message: `Update successful, id= ${req.params.id}`});
});

module.exports = router; // export default router;
