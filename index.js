const express = require('express');
const cors = require('cors');
const places = require('./app/places');
const categories = require('./app/categories');
const subjects = require('./app/subjects');
const mysqlDb = require('./mysqlDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));
const port = 8000;

app.use('/places', places);
app.use('/categories', categories);
app.use('/subjects', subjects);

mysqlDb.connect().catch(e => console.log(e));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
