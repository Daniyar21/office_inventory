drop database if exists office_inventory;
create database if not exists office_inventory;

use office_inventory;

create table if not exists categories (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists places (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists subjects (
    id int not null auto_increment primary key,
    category_id int not null,
    place_id int not null,
    title varchar(255) not null,
    description text null,
    image varchar(255) null,
    constraint subject_category_id_fk
    foreign key(category_id)
    references categories(id),
    constraint subject_place_id_fk
    foreign key(place_id)
    references places(id)
    on update cascade
    on delete restrict
);

insert into categories (title, description)
values ('furniture', 'Some stuff for office'),
       ('equipment', 'Some stuff for computer'),
       ('appliances', 'Some stuff for home');

insert into places (title, description)
values ('office of director', 'Where Boss sitting (lol)'),
       ('office 204', 'Our rest room'),
       ('room of teachers', 'Where other bosses sitting');

insert into subjects (category_id, place_id, title, description, image)
values (2, 1, 'Laptop HP 450','Powerful laptop', null),
       (1, 3, 'Octopus leather sofa', 'True octopus leather', null),
       (3, 2, 'Kettle', 'Kettle is kettle in Africa', null);
select * from subjects;



